﻿using System;
using ContactManager.Core.Models;
using ContactManager.Core.Services.Interfaces;

namespace ContactManager.Core.ViewModels
{
    public class MainViewModel
    {
        public IDataBase ContactManagerDB { get; set; }
        public IAlertDialog AlertDialog { get; set; }
        public ISubMenu SubMenu { get; set; }

        public MainViewModel(IDataBase database, IAlertDialog alertDialog, ISubMenu subMenu)
        {
            ContactManagerDB = database;
            AlertDialog = alertDialog;
            SubMenu = subMenu;
        }

        public EventHandler<Navigator> NavigateTo { get; set; }

        public void NavigateToGroups()
        {
            NavigateTo.Invoke(this, new Navigator(NavigatorDestiny.GroupViewController, null));
        }

        public void NavigateToContactDetail(Contact contact)
        {
            NavigateTo.Invoke(this, new Navigator(NavigatorDestiny.ContactDetailViewController, contact));
        }

        public void NavigateToContactEdit(Contact contact)
        {
            NavigateTo.Invoke(this, new Navigator(NavigatorDestiny.ContactEditViewController, contact));
        }

        public void NavigateToNewContact()
        {
            NavigateTo.Invoke(this, new Navigator(NavigatorDestiny.NewContactViewController, null));
        }

        public void NavigateToGroupDetail(Group group)
        {
            NavigateTo.Invoke(this, new Navigator(NavigatorDestiny.GroupDetailViewController, group));
        }

        public void NavigateToGroupEdit(Group group)
        {
            NavigateTo.Invoke(this, new Navigator(NavigatorDestiny.GroupEditViewController, group));
        }

        public void NavigateToNewGroup()
        {
            NavigateTo.Invoke(this, new Navigator(NavigatorDestiny.NewGroupViewController, null));
        }

        public void CallNumber(string number)
        {
            //TODO: make a call
            AlertDialog.CreateAlertDialog("Calling...", "Calling " + number, () => { });
        }

        public void SendContactSMS(Contact contact)
        {
            //TODO: sens SMS
            AlertDialog.CreateAlertDialog("Sending SMS...", "Message sent!", () => { });
        }

        public void SendContactEmail(Contact contact)
        {
            //TODO: sensd Email
            AlertDialog.CreateAlertDialog("Sending Email...", "Email sent!", () => { });
        }

        public void ShareContact(Contact contact)
        {
            //TODO: share
            AlertDialog.CreateAlertDialog("Sharing...", "Contact shared!", () => {});
        }

        public void SendGroupSMS(Group group)
        {
            var contactsNumbers = group.GetContactsNumbers();
            AlertDialog.CreateAlertDialog("Sending SMS...", "Message sent to " + contactsNumbers.Length + " numbers!", () => { });
        }

        public void SendGroupEmail(Group group)
        {
            var contactsEmails = group.GetContactsEmails();
            AlertDialog.CreateAlertDialog("Sending Email...", "Email sent to " + contactsEmails.Length + " email accounts!", () => { });
        }
    }
}