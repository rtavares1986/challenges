﻿using ContactManager.Core.Models;
using ContactManager.Core.Services;

namespace ContactManager.Core.ViewModels
{
    public class GroupDetailViewModel : ObservableObject
    {
        private MainViewModel _mainViewModel;

        public GroupDetailViewModel(MainViewModel mainViewModel)
        {
            _mainViewModel = mainViewModel;
        }

        public void OnEditGroupButtonClick(Group group)
        {
            _mainViewModel.NavigateToGroupEdit(group);
        }

        public void OnRemoveGroupButtonClick(Group group)
        {

            _mainViewModel.AlertDialog.CreateAlertDialog(
                title: "Remove Contact",
                text: "Do you want to remove '" + group.Name + "' group?",
                onOkClick: () =>
                {
                    _mainViewModel.ContactManagerDB.DeleteGroup(group);
                    RaisePropertyChanged("PopViewController");
                }
            );
        }

        public void CallContact(Contact contact)
        {
            _mainViewModel.CallNumber(contact.GetPreferredNumber());
        }
    }
}
