﻿using ContactManager.Core.Models;
using ContactManager.Core.Services;

namespace ContactManager.Core.ViewModels
{
    public class GroupEditViewModel : ObservableObject
    {
        private MainViewModel _mainViewModel;

        public GroupEditViewModel(MainViewModel mainViewModel)
        {
            _mainViewModel = mainViewModel;
        }

        public Contact[] GetAllContacts()
        {
            return _mainViewModel.ContactManagerDB.GetContacts().ToArray();
        }

        public void SaveGroup(Group group)
        {
            _mainViewModel.ContactManagerDB.InsertUpdateGroup(group);
        }
    }
}