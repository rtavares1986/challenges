﻿using System;
using ContactManager.Core.Models;
using ContactManager.Core.Services;

namespace ContactManager.Core.ViewModels
{
    public class ContactViewModel : ObservableObject
    {
        private MainViewModel _mainViewModel;
        public ContactFilter ContactFilter { get; set; }

        public ContactViewModel(MainViewModel mainViewModel, ContactFilter contactFilter)
        {
            _mainViewModel = mainViewModel;
            ContactFilter = contactFilter;
        }

        public void OnGroupButtonClick()
        {
            _mainViewModel.NavigateToGroups();
        }

        public void OnContactDetailButtonClick(Contact contact)
        {
            _mainViewModel.NavigateToContactDetail(contact);
        }

        public void OnContactEditButtonClick(Contact contact)
        {
            _mainViewModel.NavigateToContactEdit(contact);
        }

        public void OnAddContactButtonClick()
        {
            _mainViewModel.NavigateToNewContact();
        }

        public void OnRemoveContactButtonClick(Contact contact)
        {
            _mainViewModel.AlertDialog.CreateAlertDialog(
                title: "Remove Contact",
                text: "Do you want to remove '" + contact.Name + "' contact?",
                onOkClick: () =>
                {
                    _mainViewModel.ContactManagerDB.DeleteContact(contact);
                    ContactFilter.RefreshFilter();
                    RaisePropertyChanged("RefreshController");
                }
            );
        }

        public void OnSubMenuClick(Contact contact)
        {
            _mainViewModel.SubMenu.CreateSubMenu(
                tag: 0xA354EB2,
                options: new string[] { "Edit", "Send Message", "Share", "Remove" },
                actions: new Action[]
                {
                    () => OnContactEditButtonClick(contact),
                    () => _mainViewModel.SendContactSMS(contact),
                    () => _mainViewModel.ShareContact(contact),
                    () => OnRemoveContactButtonClick(contact)
                }
            );
        }

        public void OnSearchBarTextChange(string filter)
        {
            ContactFilter.Filter = filter;
            RefreshController();
        }

        public void CallContact(Contact contact)
        {
            _mainViewModel.CallNumber(contact.GetPreferredNumber());
        }

        public void RefreshController()
        {
            ContactFilter.RefreshFilter();
            RaisePropertyChanged("RefreshController");
        }
    }
}