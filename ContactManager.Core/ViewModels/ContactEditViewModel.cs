﻿using ContactManager.Core.Models;
using ContactManager.Core.Services;
using ContactManager.Core.Services.Interfaces;

namespace ContactManager.Core.ViewModels
{
    public class ContactEditViewModel : ObservableObject
    {
        private IDataBase _contactDB;

        public ContactEditViewModel(IDataBase contactDB)
        {
            _contactDB = contactDB;
        }

        public void SaveContact(Contact contact)
        {
            _contactDB.InsertUpdateContact(contact);
            RaisePropertyChanged("PopViewController");
        }
    }
}