﻿using ContactManager.Core.Models;
using ContactManager.Core.Services;

namespace ContactManager.Core.ViewModels
{
    public class ContactDetailViewModel : ObservableObject
    {
        private MainViewModel _mainViewModel;

        public ContactDetailViewModel(MainViewModel mainViewModel)
        {
            _mainViewModel = mainViewModel;
        }

        public void OnEditContactButtonClick(Contact contact)
        {
            _mainViewModel.NavigateToContactEdit(contact);
        }

        public void OnRemoveContactButtonClick(Contact contact)
        {
            _mainViewModel.AlertDialog.CreateAlertDialog(
                title: "Remove Contact",
                text: "Do you want to remove '" + contact.Name + "' contact?",
                onOkClick: () =>
                {
                    _mainViewModel.ContactManagerDB.DeleteContact(contact);
                    RaisePropertyChanged("PopViewController");
                }
            );
        }

        public void CallNumber(string number)
        {
            _mainViewModel.CallNumber(number);
        }

        public void ShareContact(Contact contact)
        {
            _mainViewModel.ShareContact(contact);
        }

        public void SendEmail(Contact contact)
        {
            _mainViewModel.SendContactEmail(contact);
        }
    }
}