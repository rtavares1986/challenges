﻿using System;
using System.Collections.Generic;
using ContactManager.Core.Models;
using ContactManager.Core.Services;

namespace ContactManager.Core.ViewModels
{
    public class GroupViewModel : ObservableObject
    {
        private MainViewModel _mainViewModel;

        public GroupViewModel(MainViewModel mainViewModel)
        {
            _mainViewModel = mainViewModel;
        }

        public void OnAddGroupButtonClick()
        {
            _mainViewModel.NavigateToNewGroup();
        }

        public void OnGroupDetailButtonClick(Group group)
        {
            _mainViewModel.NavigateToGroupDetail(group);
        }

        public void OnGroupEditButtonClick(Group group)
        {
            _mainViewModel.NavigateToGroupEdit(group);
        }

        public void OnRemoveGroupButtonClick(Group group)
        {
            _mainViewModel.AlertDialog.CreateAlertDialog(
                title: "Remove Group",
                text: "Do you want to remove '" + group.Name + "' group?",
                onOkClick: () =>
                {
                    _mainViewModel.ContactManagerDB.DeleteGroup(group);
                    RaisePropertyChanged("RefreshController");
                }
            );
        }

        public void OnSubMenuClick(Group group)
        {
            _mainViewModel.SubMenu.CreateSubMenu(
                tag: 0xA354EB2,
                options: new string[] { "Edit", "Remove" },
                actions: new Action[]
                {
                    () => OnGroupEditButtonClick(group),
                    () => OnRemoveGroupButtonClick(group)
                }
            );
        }

        public List<Group> GetGroups()
        {
            return _mainViewModel.ContactManagerDB.GetGroups();
        }

        public void SendGroupSMS(Group group)
        {
            _mainViewModel.SendGroupSMS(group);
        }

        public void SendGroupEmail(Group group)
        {
            _mainViewModel.SendGroupEmail(group);
        }
    }
}