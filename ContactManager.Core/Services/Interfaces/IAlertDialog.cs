﻿using System;

namespace ContactManager.Core.Services.Interfaces
{
    public interface IAlertDialog
    {
        void CreateAlertDialog(string title, string text, Action onOkClick);
    }
}
