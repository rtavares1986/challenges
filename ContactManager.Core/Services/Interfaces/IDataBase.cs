﻿using System.Collections.Generic;
using ContactManager.Core.Models;

namespace ContactManager.Core.Services.Interfaces
{
    public interface IDataBase
    {
        void LoadDB();
        List<Contact> GetContacts();
        void InsertUpdateContact(Contact contact);
        void DeleteContact(Contact contact);
        List<Group> GetGroups();
        void InsertUpdateGroup(Group group);
        void DeleteGroup(Group group);
    }
}