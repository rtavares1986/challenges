﻿using System;
namespace ContactManager.Core.Services.Interfaces
{
    public interface ISubMenu
    {
        void CreateSubMenu(int tag, string[] options, Action[] actions);
    }
}