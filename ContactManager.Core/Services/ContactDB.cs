﻿using System.Collections.Generic;
using ContactManager.Core.Models;

namespace ContactManager.Core.Services
{
    public class ContactDB
    {
        public List<Contact> Contacts { get; private set; }

        public ContactDB()
        {
            Contacts = new List<Contact>();
        }

        public ContactDB(List<Contact> contacts)
        {
            Contacts = contacts;
        }

        public void InsertUpdateContact(Contact contact)
        {
            if (!Contacts.Contains(contact))
                Contacts.Add(contact);
        }

        public void DeleteContact(Contact contact)
        {
            Contacts.Remove(contact);
        }
    }
}