﻿using System.Collections.Generic;
using ContactManager.Core.Models;

namespace ContactManager.Core.Services
{
    public class GroupDB
    {
        public List<Group> Groups { get; private set; }

        public GroupDB()
        {
            Groups = new List<Group>();
        }

        public GroupDB(List<Group> groups)
        {
            Groups = groups;
        }

        public void InsertUpdateGroup(Group group)
        {
            if (!Groups.Contains(group))
                Groups.Add(group);
        }

        public void DeleteGroup(Group group)
        {
            Groups.Remove(group);
        }
    }
}
