﻿using System.Collections.Generic;
using ContactManager.Core.Models;
using ContactManager.Core.Services.Interfaces;
using Newtonsoft.Json;

namespace ContactManager.Core.Services
{
    public class ContactManagerDB : IDataBase
    {
        private ContactDB _contactDB;
        private GroupDB _groupDB;

        public ContactManagerDB()
        {
            CreateInstance();
            //LoadDB();
        }

        public List<Contact> GetContacts()
        {
            return _contactDB.Contacts;
        }

        public List<Group> GetGroups()
        {
            return _groupDB.Groups;
        }

        public void LoadDB()
        {
            /*
            var defaults = NSUserDefaults.StandardUserDefaults;
            string contactsJson = defaults.StringForKey("ContactDB");
            string groupsJson = defaults.StringForKey("GroupDB");
            if ((contactsJson ?? "").Length == 0 || (groupsJson ?? "").Length == 0)
            {
                CreateInstance();
                return;
            }
            _contactDB = JsonConvert.DeserializeObject<ContactDB>(contactsJson);
            _groupDB = JsonConvert.DeserializeObject<GroupDB>(groupsJson);
            */
        }

        public void CreateInstance()
        {
            var tel10 = new Telephone("Mobile", "914567892", true);
            var tel11 = new Telephone("Home", "214452525", false);
            var tel12 = new Telephone("Work", "215343537", false);
            var tel20 = new Telephone("Mobile", "962454241", true);
            var tel21 = new Telephone("Home", "213582743", false);
            var tel30 = new Telephone("Mobile", "925646456", true);
            var tel40 = new Telephone("Mobile", "913546138", true);

            var contacts = new List<Contact> {
                new Contact("", "Carlos Ribeiro", "Rua Horta Seca 3", (38.710315, -9.143905), new List<Telephone> { tel10, tel11, tel12 }, "carlos@example.com" ),
                new Contact("", "Manuel Santos", "Rua Alexandre Herculano 40", (38.722164, -9.149863), new List<Telephone> { tel20, tel21 }, "manuel@example.com" ),
                new Contact("", "Rui Pereira", "Rua Olivério Serpa 9", (38.749613, -9.207854), new List<Telephone> { tel30 }, "rui@example.com" ),
                new Contact("", "Ricardo Amaral", "Avenida Câmara Pestana 15", (38.754110, -9.214555), new List<Telephone> { tel40 }, "ricardo@example.com" )
            };
            _contactDB = new ContactDB(contacts);

            Group group1 = new Group("Familia");
            group1.AddContact(contacts[0]);
            group1.AddContact(contacts[1]);
            Group group2 = new Group("Trabalho");
            group2.AddContact(contacts[2]);
            group2.AddContact(contacts[3]);
            _groupDB = new GroupDB(new List<Group> { group1, group2 });
        }

        private void SaveDB()
        {
            /*
            var defaults = NSUserDefaults.StandardUserDefaults;
            string contactsJson = JsonConvert.SerializeObject(_contactDB);
            string groupsJson = JsonConvert.SerializeObject(_groupDB);
            defaults.SetString(contactsJson, "ContactDB");
            defaults.SetString(groupsJson, "GroupDB");
            */
        }

        public void InsertUpdateContact(Contact contact)
        {
            _contactDB.InsertUpdateContact(contact);
            SaveDB();
        }

        public void DeleteContact(Contact contact)
        {
            _contactDB.DeleteContact(contact);
            SaveDB();
        }

        public void InsertUpdateGroup(Group group)
        {
            _groupDB.InsertUpdateGroup(group);
            SaveDB();
        }

        public void DeleteGroup(Group group)
        {
            _groupDB.DeleteGroup(group);
            SaveDB();
        }
    }
}