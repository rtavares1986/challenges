﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Essentials;

namespace ContactManager.Core.Services
{
    public abstract class ObservableObject : INotifyPropertyChanged
    {
        private static readonly PropertyChangedEventArgs AllPropertiesChanged = new PropertyChangedEventArgs(string.Empty);

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string name = "") => RaisePropertyChanged(new PropertyChangedEventArgs(name));

        public virtual void RaiseAllPropertiesChanged() => RaisePropertyChanged(AllPropertiesChanged);

        public void RaisePropertyChanged(PropertyChangedEventArgs changedArgs) => MainThread.BeginInvokeOnMainThread(
            () => PropertyChanged?.Invoke(this, changedArgs));

        protected virtual bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
                return false;
            storage = value;
            RaisePropertyChanged(propertyName);
            return true;
        }
    }
}