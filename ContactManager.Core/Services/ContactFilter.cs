﻿using System.Collections.Generic;
using System.Linq;
using ContactManager.Core.Models;
using ContactManager.Core.Services.Interfaces;

namespace ContactManager.Core.Services
{
    public class ContactFilter
    {
        private IDataBase _contactDB;
        public string Filter { get; set; }
        public Dictionary<string, List<Contact>> IndexedTableItems { get; private set; }
        public string[] Keys { get; private set; }

        public ContactFilter(IDataBase contactDB)
        {
            _contactDB = contactDB;
            Filter = "";

            RefreshFilter();
        }

        public void RefreshFilter()
        {
            List<Contact> filteredContacts = new List<Contact>();
            foreach (Contact contact in _contactDB.GetContacts())
                if (contact.Name.ToLower().Contains((Filter ?? "").ToLower()))
                    filteredContacts.Add(contact);
            filteredContacts.Sort();

            IndexedTableItems = new Dictionary<string, List<Contact>>();
            foreach (Contact contact in filteredContacts)
            {
                string name = contact.Name;
                if (IndexedTableItems.ContainsKey(name[0].ToString()))
                    IndexedTableItems[name[0].ToString()].Add(contact);
                else
                    IndexedTableItems.Add(name[0].ToString(), new List<Contact>() { contact });
            }
            Keys = IndexedTableItems.Keys.ToArray();
        }
    }
}