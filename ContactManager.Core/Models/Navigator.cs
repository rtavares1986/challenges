﻿namespace ContactManager.Core.Models
{
    public enum NavigatorDestiny
    {
        GroupViewController,
        ContactDetailViewController,
        ContactEditViewController,
        NewContactViewController,
        GroupDetailViewController,
        GroupEditViewController,
        NewGroupViewController
    }

    public class Navigator
    {
        public NavigatorDestiny Key { get; set; }
        public object Parameter { get; set; }

        public Navigator(NavigatorDestiny key, object parameter)
        {
            Key = key;
            Parameter = parameter;
        }
    }
}