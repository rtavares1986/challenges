﻿using System;
using System.Collections.Generic;

namespace ContactManager.Core.Models
{
    public class Contact : IComparable<Contact>
    {
        public string Photo { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public (double, double) Location { get; set; }
        public List<Telephone> Telephones { get; set; }
        public string Email { get; set; }

        public Contact()
        {
            Photo = "";
            Name = "";
            Address = "";
            Location = (0, 0);
            Telephones = new List<Telephone>();
            Email = "";
        }

        public Contact(string filename, string name, string address,
                 (double, double) location, List<Telephone> telephones, string email)
        {
            Photo = filename ?? "";
            Name = name;
            Address = address;
            Location = location;
            Telephones = telephones;
            Email = email;
        }

        public string GetInitials()
        {
            if ((Name ?? "").Length == 0)
                return null;

            string[] nameSurname = Name.Trim().Split(' ');
            if (nameSurname.Length > 1)
            {
                char n = nameSurname[0][0];
                char sn = nameSurname[nameSurname.Length-1][0];
                return n.ToString() + sn.ToString();
            }
            return Name.Substring(0, 2);
        }

        private Telephone GetPreferedTelephone()
        {
            foreach (Telephone tel in Telephones)
            {
                if (tel.Preferred)
                    return tel;
            }
            return null;
        }

        public string GetPreferredNumber()
        {
            return GetPreferedTelephone()?.Number ?? "<Empty>";
        }

        override public string ToString()
        {
            return GetPreferredNumber();
        }

        public int CompareTo(Contact other)
        {
            return Name.CompareTo(other.Name);
        }
    }
}