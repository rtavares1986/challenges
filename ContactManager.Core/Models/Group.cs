﻿using System.Collections.Generic;
using System.Linq;

namespace ContactManager.Core.Models
{
    public class Group
    {
        public string Name { get; set; }
        public List<Contact> Contacts { get; set; }

        public Group(string name)
        {
            Name = name;
            Contacts = new List<Contact>();
        }

        public void AddContact(Contact contact)
        {
            Contacts.Add(contact);
        }

        public void RemoveContact(Contact contact)
        {
            Contacts.Remove(contact);
        }

        public string[] GetContactsNumbers() {
            return Contacts.Select(contact => contact.GetPreferredNumber()).ToList().ToArray();
        }

        public string[] GetContactsEmails()
        {
            return Contacts.Select(contact => contact.Email).ToList().ToArray();
        }
    }
}