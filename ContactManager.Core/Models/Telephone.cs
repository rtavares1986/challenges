﻿namespace ContactManager.Core.Models
{
    public class Telephone
    {
        public string Name { get; set; }
        public string Number { get; set; }
        public bool Preferred { get; set; }

        public Telephone(string name, string number, bool preferred)
        {
            Name = name;
            Number = number;
            Preferred = preferred;
        }

        public Telephone Clone() {
            return new Telephone(Name, Number, Preferred);
        }
    }
}