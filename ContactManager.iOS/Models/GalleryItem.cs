﻿using System;
using UIKit;

namespace ContactManager.iOS.Models
{
    public class GalleryItem
    {
        public string Title { get; set; }
        public UIImage Image { get; set; }
        public Action<object> ImageAction { get; set; }
        public object ImageActionParameter { get; set; }

        public GalleryItem(string title, UIImage image, Action<object> imageAction, object imageActionParameter)
        {
            Title = title;
            Image = image;
            ImageAction = imageAction;
            ImageActionParameter = imageActionParameter;
        }
    }
}