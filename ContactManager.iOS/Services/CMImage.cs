﻿using System;
using System.IO;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace ContactManager.iOS.Services
{
    public class CMImage
    {
        async public static Task<string> GetPhotoFrom()
        {
            try
            {
                FileResult photo = await MediaPicker.PickPhotoAsync();
                if (photo == null)
                    return null;
                string photoPath = Path.Combine(FileSystem.CacheDirectory, photo.FileName);
                using (var stream = await photo.OpenReadAsync())
                using (var newStream = File.OpenWrite(photoPath))
                    await stream.CopyToAsync(newStream);

                return photoPath;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }
}