﻿using System;
using ContactManager.Core.Models;
using CoreGraphics;
using UIKit;

namespace ContactManager.iOS.Services
{
    public class Util
    {
        public static UIImage GetPhoto(Contact contact)
        {
            if (contact.Photo.Length != 0)
                return UIImage.FromBundle(contact.Photo);
            string initials = contact.GetInitials();
            if (initials == null)
                return UIImage.FromBundle("contact_icon");
            return CreateLettersImage(initials);
        }

        public static UIImage CreateLettersImage(string letters)
        {
            var frame = new CGRect(x: 0, y: 0, width: 100, height: 100);
            UILabel nameLabel = new UILabel(frame: frame);
            nameLabel.TextAlignment = UITextAlignment.Center;
            nameLabel.BackgroundColor = UIColor.LightGray;
            nameLabel.TextColor = UIColor.White;
            nameLabel.Font = UIFont.BoldSystemFontOfSize(40);
            nameLabel.Text = letters;

            UIGraphics.BeginImageContext(frame.Size);
            var currentContext = UIGraphics.GetCurrentContext();
            nameLabel.Layer.RenderInContext(currentContext);
            var nameImage = UIGraphics.GetImageFromCurrentImageContext();
            return nameImage;
        }

        public static UIBarButtonItem CreateBarItem(string title = null, string customIcon = null, string systemIcon = null, Action action = null)
        {
            UIBarButtonItem btn = new UIBarButtonItem();
            if (title != null)
                btn.Title = title;
            if (customIcon != null)
            {
                UIImage image = UIImage.FromBundle(customIcon);
                btn.Image = image;
            }
            if (systemIcon != null)
            {
                UIImage image = Util.GetSystemImage(systemIcon);
                
                btn.Image = image;
            }
            if (action != null)
                btn.Clicked += (sender, e) => action();
            return btn;
        }

        public static UIImage GetSystemImage(string systemIcon)
        {
            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
                return UIImage.GetSystemImage(systemIcon);
            else
            {
                var imageName = systemIcon.Replace(".", "_");
                UIImage image = UIImage.FromBundle(imageName);
                return ResizeImage(image, 30, 30);
            }
        }

        public static void MessageDialog(string title, string text)
        {
            UIAlertController okAlertController = UIAlertController.Create(title, text, UIAlertControllerStyle.Alert);
            okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
            var controller = UIApplication.SharedApplication.KeyWindow.RootViewController;
            controller.PresentViewController(okAlertController, true, null);
        }

        public static UIImage ResizeImage(UIImage image, nfloat width, nfloat height) {
            var size = image.Size;

            var widthRatio = width / size.Width;
            var heightRatio = height / size.Height;

            CGSize newSize;
            if (widthRatio > heightRatio)
                newSize = new CGSize(size.Width * heightRatio, size.Height * heightRatio);
            else
                newSize = new CGSize(size.Width * widthRatio, size.Height * widthRatio);

            var rect = new CGRect(0, 0, newSize.Width, newSize.Height);

            UIGraphics.BeginImageContextWithOptions(newSize, false, 1);
            image.Draw(rect);
            var newImage = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            return newImage;
        }
    }
}