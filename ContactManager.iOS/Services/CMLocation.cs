﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace ContactManager.iOS.Services
{
    public class CMLocation
    {
        public static async Task<(double, double)> GetCurrentLocation()
        {
            try
            {
                Location location = await Geolocation.GetLastKnownLocationAsync();
                if (location == null)
                    return (0, 0);
                return (location.Latitude, location.Longitude);
            }
            catch (Exception) { }
            return (0, 0);
        }

        public static async Task<string> GetAddress(double latitude, double longitude)
        {
            try
            {
                var placemarks = await Geocoding.GetPlacemarksAsync(latitude, longitude);
                Placemark placemark = placemarks?.FirstOrDefault();
                if (placemark != null)
                    return placemark.FeatureName;
            }
            catch (Exception) { }
            return "";
        }
    }
}