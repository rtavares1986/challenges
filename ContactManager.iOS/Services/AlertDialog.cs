﻿using System;
using UIKit;
using ContactManager.Core.Services.Interfaces;

namespace ContactManager.iOS.Services
{
    public class AlertDialog : IAlertDialog
    {
        public void CreateAlertDialog(string title, string text, Action onOkClick)
        {
            UIAlertController okAlertController = UIAlertController.Create(title, text, UIAlertControllerStyle.Alert);
            okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, (_) => onOkClick()));
            okAlertController.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Default, null));
            var controller = UIApplication.SharedApplication.KeyWindow.RootViewController;
            controller.PresentViewController(okAlertController, true, null);
        }
    }
}