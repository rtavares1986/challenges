﻿using System;
using System.Runtime.CompilerServices;
using ContactManager.Core.Services.Interfaces;
using ContactManager.iOS.Sources;
using CoreGraphics;
using Foundation;
using UIKit;

namespace ContactManager.iOS.Services
{
    public class SubMenu : ISubMenu
    {
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void CreateSubMenu(int tag, string[] options, Action[] actions)
        {
            var window = UIApplication.SharedApplication.KeyWindow;
            float height = options.Length*50;
            if (height > 250)
                height = 250;

            var transparentView = window.ViewWithTag(tag);
            if (transparentView != null)
                return;
            transparentView = new UIView();
            transparentView.Tag = tag;
            transparentView.BackgroundColor = UIColor.Black.ColorWithAlpha(0.9f);
            transparentView.Frame = window.Frame;
            transparentView.Alpha = 0;
            window?.AddSubview(transparentView);

            var screenSize = UIScreen.MainScreen.Bounds.Size;
            var tableView = new UITableView();
            tableView.RegisterNibForCellReuse(UINib.FromName("MenuViewCell", NSBundle.MainBundle), "MenuViewCell");
            tableView.Frame = new CGRect(x: 0, y: screenSize.Height, width: screenSize.Width, height: height);
            tableView.Source = new SubMenuSource(options, actions, () => {
                CloseMenu(transparentView, tableView, screenSize, height);
            });
            window?.AddSubview(tableView);

            transparentView.AddGestureRecognizer(new UITapGestureRecognizer(() => {
                CloseMenu(transparentView, tableView, screenSize, height);
            }));

            OpenMenu(transparentView, tableView, screenSize, height);
        }

        private static void OpenMenu(UIView transparentView, UITableView tableView, CGSize screenSize, float height)
        {
            UIView.Animate(duration: 0.3, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animation: () => {
                tableView.Frame = new CGRect(x: 0, y: screenSize.Height - height, width: screenSize.Width, height: height);
                transparentView.Alpha = 0.5f;
            }, completion: null);
        }

        private static void CloseMenu(UIView transparentView, UITableView tableView, CGSize screenSize, float height)
        {
            UIView.Animate(duration: 0.3, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animation: () => {
                tableView.Frame = new CGRect(x: 0, y: screenSize.Height, width: screenSize.Width, height: height);
                transparentView.Alpha = 0;
            }, completion: () => {
                tableView.RemoveFromSuperview();
                transparentView.RemoveFromSuperview();
            });
        }
    }
}