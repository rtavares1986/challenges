﻿using System;
using Foundation;
using UIKit;

namespace ContactManager.iOS.Views
{
    public partial class ContactViewCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("ContactViewCell");
        public static readonly UINib Nib;
        private EventHandler callButtonEventHandler;
        private UITapGestureRecognizer _tapGesture;
        private UILongPressGestureRecognizer _longPressGesture;

        static ContactViewCell()
        {
            Nib = UINib.FromName("ContactViewCell", NSBundle.MainBundle);
        }

        protected ContactViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void PrepareForReuse()
        {
            if (callButtonEventHandler != null)
            {
                contactCall.TouchUpInside -= callButtonEventHandler;
                callButtonEventHandler = null;
            }
            if (_tapGesture != null)
            {
                RemoveGestureRecognizer(_tapGesture);
                _tapGesture = null;
            }
            if (_longPressGesture != null)
            {
                RemoveGestureRecognizer(_longPressGesture);
                _longPressGesture = null;
            }
        }

        public void SetPhotoImage(UIImage image)
        {
            contactPhoto.Image = image;
        }

        public void SetName(string text)
        {
            contactName.Text = text;
        }

        public void SetNumber(string text)
        {
            contactTelephone.Text = text;
        }

        public void SetCallButtonAction(Action action)
        {
            callButtonEventHandler = (sender, e) => action();
            contactCall.TouchUpInside += callButtonEventHandler;
        }

        public void SetOnCellTapAction(Action action)
        {
            _tapGesture = new UITapGestureRecognizer(action);
            AddGestureRecognizer(_tapGesture);
        }

        public void SetOnCellLongClickAction(Action action)
        {
            _longPressGesture = new UILongPressGestureRecognizer(action);
            AddGestureRecognizer(_longPressGesture);
        }
    }
}