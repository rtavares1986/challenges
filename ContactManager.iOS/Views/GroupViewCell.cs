﻿using System;
using Foundation;
using UIKit;

namespace ContactManager.iOS.Views
{
    public partial class GroupViewCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("GroupViewCell");
        public static readonly UINib Nib;
        private EventHandler _messageEventHandler;
        private EventHandler _emailEventHandler;
        private UITapGestureRecognizer _tapGesture;
        private UILongPressGestureRecognizer _longPressGesture;

        static GroupViewCell()
        {
            Nib = UINib.FromName("GroupViewCell", NSBundle.MainBundle);
        }

        protected GroupViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void PrepareForReuse()
        {
            if (_messageEventHandler != null)
            {
                groupMessage.TouchUpInside -= _messageEventHandler;
                _messageEventHandler = null;
            }
            if (_emailEventHandler != null)
            {
                groupEmail.TouchUpInside -= _emailEventHandler;
                _emailEventHandler = null;
            }
            if (_tapGesture != null)
            {
                RemoveGestureRecognizer(_tapGesture);
                _tapGesture = null;
            }
            if (_longPressGesture != null)
            {
                RemoveGestureRecognizer(_longPressGesture);
                _longPressGesture = null;
            }
        }

        public void SetName(string name)
        {
            groupName.Text = name;
        }

        public void SetMessageAction(Action action)
        {
            _messageEventHandler = (sender, e) => action();
            groupMessage.TouchUpInside += _messageEventHandler;
        }

        public void SetEmailAction(Action action)
        {
            _emailEventHandler = (sender, e) => action();
            groupEmail.TouchUpInside += _emailEventHandler;
        }

        public void SetOnCellTapAction(Action action)
        {
            _tapGesture = new UITapGestureRecognizer(action);
            AddGestureRecognizer(_tapGesture);
        }

        public void SetOnCellLongClickAction(Action action)
        {
            _longPressGesture = new UILongPressGestureRecognizer(action);
            AddGestureRecognizer(_longPressGesture);
        }
    }
}