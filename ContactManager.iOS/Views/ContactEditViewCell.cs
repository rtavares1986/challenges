﻿using System;
using Foundation;
using UIKit;
using ContactManager.iOS.Services;

namespace ContactManager.iOS.Views
{
    public partial class ContactEditViewCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("ContactEditViewCell");
        public static readonly UINib Nib;
        private EventHandler _editNameActionEventHandler;
        private EventHandler _editNumberActionEventHandler;
        private EventHandler _removeNumberActionEventHandler;
        private EventHandler _preferredNumberActionEventHandler;

        static ContactEditViewCell()
        {
            Nib = UINib.FromName("ContactEditViewCell", NSBundle.MainBundle);
        }

        protected ContactEditViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void PrepareForReuse()
        {
            if (_editNameActionEventHandler != null)
            {
                telName.TouchUpInside -= _editNameActionEventHandler;
                _editNameActionEventHandler = null;
            }
            if (_editNumberActionEventHandler != null)
            {
                telNumber.EditingChanged -= _editNumberActionEventHandler;
                _editNumberActionEventHandler = null;
            }
            if (_removeNumberActionEventHandler != null)
            {
                telRemove.TouchUpInside -= _removeNumberActionEventHandler;
                _removeNumberActionEventHandler = null;
            }
            if (_preferredNumberActionEventHandler != null)
            { 
                telStar.TouchUpInside -= _preferredNumberActionEventHandler;
                _preferredNumberActionEventHandler = null;
            }
        }

        public string GetName()
        {
            return telName.Title(UIControlState.Normal);
        }

        public void SetName(string name)
        {
            telName.SetTitle(name, UIControlState.Normal);
        }

        public void SetEditNameAction(Action<string> action)
        {
            _editNameActionEventHandler = (sender, e) => action(telNumber.Text);
            telName.TouchUpInside += _editNameActionEventHandler;
        }

        public string GetNumber()
        {
            return telNumber.Text;
        }

        public void SetNumber(string number)
        {
            telNumber.Text = number;
        }

        public void SetEditNumberAction(Action<string> action)
        {
            _editNumberActionEventHandler = (sender, e) => action(telNumber.Text);
            telNumber.EditingChanged += _editNumberActionEventHandler;
        }

        public void SetRemoveIcon(UIImage icon)
        {
            telRemove.SetImage(icon, UIControlState.Normal);
        }

        public void SetRemoveNumberAction(Action action)
        {
            _removeNumberActionEventHandler = (sender, e) => action();
            telRemove.TouchUpInside += _removeNumberActionEventHandler;
        }

        public bool GetPreferredNumber()
        {
            return telStar.CurrentImage == Util.GetSystemImage("star.fill");
        }

        public void SetPreferredNumber(bool preferred)
        {
            if (preferred)
                telStar.SetImage(Util.GetSystemImage("star.fill"), UIControlState.Normal);
            else
                telStar.SetImage(Util.GetSystemImage("star"), UIControlState.Normal);
        }

        public void SetPreferredNumberAction(Action action)
        {
            _preferredNumberActionEventHandler = (sender, e) => action();
            telStar.TouchUpInside += _preferredNumberActionEventHandler;
        }
    }
}