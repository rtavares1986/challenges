// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ContactManager.iOS.Views
{
	[Register ("ContactViewCell")]
	partial class ContactViewCell
	{
		[Outlet]
		UIKit.UIButton contactCall { get; set; }

		[Outlet]
		UIKit.UIImageView contactPhoto { get; set; }

		[Outlet]
		UIKit.UILabel contactName { get; set; }

		[Outlet]
		UIKit.UILabel contactTelephone { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (contactCall != null) {
				contactCall.Dispose ();
				contactCall = null;
			}

			if (contactPhoto != null) {
				contactPhoto.Dispose ();
				contactPhoto = null;
			}

			if (contactName != null) {
				contactName.Dispose ();
				contactName = null;
			}

			if (contactTelephone != null) {
				contactTelephone.Dispose ();
				contactTelephone = null;
			}
		}
	}
}
