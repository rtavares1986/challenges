﻿using System;
using Foundation;
using UIKit;

namespace ContactManager.iOS.Views
{
    public partial class GroupDetailViewCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("GroupDetailViewCell");
        public static readonly UINib Nib;
        private EventHandler _preferredNumberEventHandler;

        static GroupDetailViewCell()
        {
            Nib = UINib.FromName("GroupDetailViewCell", NSBundle.MainBundle);
        }

        protected GroupDetailViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void PrepareForReuse()
        {
            if (_preferredNumberEventHandler != null)
                contactNumber.TouchUpInside -= _preferredNumberEventHandler;
        }

        public void SetName(string name)
        {
            contactName.Text = name;
        }

        public void SetPreferredNumber(string number)
        {
            contactNumber.SetTitle(number, UIControlState.Normal);
        }

        public void setPreferredNumberAction(Action action)
        {
            _preferredNumberEventHandler = (sender, e) => action();
            contactNumber.TouchUpInside += _preferredNumberEventHandler;
        }
    }
}