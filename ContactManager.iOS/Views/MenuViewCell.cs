﻿using System;
using Foundation;
using UIKit;

namespace ContactManager.iOS.Views
{
    public partial class MenuViewCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("MenuViewCell");
        public static readonly UINib Nib;

        static MenuViewCell()
        {
            Nib = UINib.FromName("MenuViewCell", NSBundle.MainBundle);
        }

        protected MenuViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public string Option
        {
            set { label.Text = value; }
        }
    }
}