﻿using System;

using Foundation;
using UIKit;

namespace ContactManager.iOS.Views
{
    public partial class GroupEditViewCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("GroupEditViewCell");
        public static readonly UINib Nib;
        private EventHandler removeActionEventHandler;

        static GroupEditViewCell()
        {
            Nib = UINib.FromName("GroupEditViewCell", NSBundle.MainBundle);
        }

        protected GroupEditViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public void SetName(string name)
        {
            contactName.Text = name;
        }

        public void SetNumber(string number)
        {
            ContactNumber.SetTitle(number, UIControlState.Normal);
        }

        public void SetRemoveIcon(UIImage icon)
        {
            contactRemove.SetImage(icon, UIControlState.Normal);
        }

        public void setRemoveAction(Action action)
        {
            if (removeActionEventHandler != null)
                contactRemove.TouchUpInside -= removeActionEventHandler;
            removeActionEventHandler = (sender, e) => action();
            contactRemove.TouchUpInside += removeActionEventHandler;
        }
    }
}