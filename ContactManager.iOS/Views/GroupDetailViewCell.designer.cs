// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ContactManager.iOS.Views
{
	[Register ("GroupDetailViewCell")]
	partial class GroupDetailViewCell
	{
		[Outlet]
		UIKit.UILabel contactName { get; set; }

		[Outlet]
		UIKit.UIButton contactNumber { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (contactName != null) {
				contactName.Dispose ();
				contactName = null;
			}

			if (contactNumber != null) {
				contactNumber.Dispose ();
				contactNumber = null;
			}
		}
	}
}
