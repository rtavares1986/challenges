﻿using System;
using Foundation;
using UIKit;

namespace ContactManager.iOS.Views
{
    public partial class GalleryViewCell : UICollectionViewCell
    {
        public static readonly NSString Key = new NSString("GalleryViewCell");
        public static readonly UINib Nib;

        static GalleryViewCell()
        {
            Nib = UINib.FromName("GalleryViewCell", NSBundle.MainBundle);
        }

        protected GalleryViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        //public override void PrepareForReuse() {}

        public void SetTitle(string title)
        {
            GalleryItemTitle.Text = title;
        }

        public void SetImage(UIImage image)
        {
            GalleryItemImage.Image = image;
        }
    }
}