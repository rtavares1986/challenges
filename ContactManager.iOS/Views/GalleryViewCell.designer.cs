// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ContactManager.iOS.Views
{
	[Register ("GalleryViewCell")]
	partial class GalleryViewCell
	{
		[Outlet]
		UIKit.UIImageView GalleryItemImage { get; set; }

		[Outlet]
		UIKit.UILabel GalleryItemTitle { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (GalleryItemTitle != null) {
				GalleryItemTitle.Dispose ();
				GalleryItemTitle = null;
			}

			if (GalleryItemImage != null) {
				GalleryItemImage.Dispose ();
				GalleryItemImage = null;
			}
		}
	}
}
