// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ContactManager.iOS.Views
{
	[Register ("GroupViewCell")]
	partial class GroupViewCell
	{
		[Outlet]
		UIKit.UIButton groupEmail { get; set; }

		[Outlet]
		UIKit.UILabel groupName { get; set; }

		[Outlet]
		UIKit.UIButton groupMessage { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (groupName != null) {
				groupName.Dispose ();
				groupName = null;
			}

			if (groupEmail != null) {
				groupEmail.Dispose ();
				groupEmail = null;
			}

			if (groupMessage != null) {
				groupMessage.Dispose ();
				groupMessage = null;
			}
		}
	}
}
