// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ContactManager.iOS.Views
{
	[Register ("GroupEditViewCell")]
	partial class GroupEditViewCell
	{
		[Outlet]
		UIKit.UILabel contactName { get; set; }

		[Outlet]
		UIKit.UIButton ContactNumber { get; set; }

		[Outlet]
		UIKit.UIButton contactRemove { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (contactName != null) {
				contactName.Dispose ();
				contactName = null;
			}

			if (ContactNumber != null) {
				ContactNumber.Dispose ();
				ContactNumber = null;
			}

			if (contactRemove != null) {
				contactRemove.Dispose ();
				contactRemove = null;
			}
		}
	}
}
