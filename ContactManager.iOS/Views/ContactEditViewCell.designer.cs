// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ContactManager.iOS.Views
{
	[Register ("ContactEditViewCell")]
	partial class ContactEditViewCell
	{
		[Outlet]
		UIKit.UIButton telName { get; set; }

		[Outlet]
		UIKit.UITextField telNumber { get; set; }

		[Outlet]
		UIKit.UIButton telRemove { get; set; }

		[Outlet]
		UIKit.UIButton telStar { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (telName != null) {
				telName.Dispose ();
				telName = null;
			}

			if (telNumber != null) {
				telNumber.Dispose ();
				telNumber = null;
			}

			if (telRemove != null) {
				telRemove.Dispose ();
				telRemove = null;
			}

			if (telStar != null) {
				telStar.Dispose ();
				telStar = null;
			}
		}
	}
}
