﻿using System;
using Foundation;
using UIKit;
using ContactManager.iOS.Services;

namespace ContactManager.iOS.Views
{
    public partial class ContactDetailViewCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("ContactDetailViewCell");
        public static readonly UINib Nib;
        private EventHandler _numberActionEventHandler;

        static ContactDetailViewCell()
        {
            Nib = UINib.FromName("ContactDetailViewCell", NSBundle.MainBundle);
        }

        protected ContactDetailViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void PrepareForReuse()
        {
            if (_numberActionEventHandler != null)
            {
                tvcNumber.TouchUpInside -= _numberActionEventHandler;
                _numberActionEventHandler = null;
            }
        }

        public void SetName(string text)
        {
            tvcName.Text = text;
        }

        public void SetNumber(string number)
        {
            tvcNumber.SetTitle(number, UIControlState.Normal);
        }

        public void SetNumberAction(Action action)
        {
            _numberActionEventHandler = (sender, e) => action();
            tvcNumber.TouchUpInside += _numberActionEventHandler;
        }

        public void SetPreferredNumber(bool preferred)
        {
            if (preferred)
                tvcStar.Image = Util.GetSystemImage("star.fill");
            else
                tvcStar.Image = null;
        }
    }
}