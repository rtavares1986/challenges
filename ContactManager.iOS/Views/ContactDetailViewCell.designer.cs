// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ContactManager.iOS.Views
{
	[Register ("ContactDetailViewCell")]
	partial class ContactDetailViewCell
	{
		[Outlet]
		UIKit.UILabel tvcName { get; set; }

		[Outlet]
		UIKit.UIButton tvcNumber { get; set; }

		[Outlet]
		UIKit.UIImageView tvcStar { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (tvcName != null) {
				tvcName.Dispose ();
				tvcName = null;
			}

			if (tvcNumber != null) {
				tvcNumber.Dispose ();
				tvcNumber = null;
			}

			if (tvcStar != null) {
				tvcStar.Dispose ();
				tvcStar = null;
			}
		}
	}
}
