﻿using System;
using System.Collections.Generic;
using ContactManager.Core.Models;
using ContactManager.Core.ViewModels;
using ContactManager.iOS.Views;
using Foundation;
using UIKit;

namespace ContactManager.iOS.Sources
{
    public class GroupDetailSource : UITableViewSource
    {
        private GroupDetailViewModel _groupDetailViewModel;
        private List<Contact> _contacts;

        public GroupDetailSource(GroupDetailViewModel groupDetailViewModel, List<Contact> contacts)
        {
            _groupDetailViewModel = groupDetailViewModel;
            _contacts = contacts;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _contacts.Count;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            GroupDetailViewCell cell = tableView.DequeueReusableCell("GroupDetailViewCell") as GroupDetailViewCell;
            Contact contact = _contacts[indexPath.Row];

            cell.SetName(contact.Name);
            cell.SetPreferredNumber(contact.GetPreferredNumber());
            cell.setPreferredNumberAction(() => _groupDetailViewModel.CallContact(contact));

            return cell;
        }
    }
}