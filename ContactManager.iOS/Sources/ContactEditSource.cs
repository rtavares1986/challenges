﻿using System;
using System.Collections.Generic;
using System.Linq;
using ContactManager.Core.Models;
using ContactManager.iOS.Services;
using ContactManager.iOS.ViewControllers;
using ContactManager.iOS.Views;
using Foundation;
using UIKit;

namespace ContactManager.iOS.Sources
{
    public class ContactEditSource : UITableViewSource
    {
        private ContactEditViewController _contactEditViewController;
        public List<Telephone> Telephones { get; private set; }

        public ContactEditSource(ContactEditViewController contactEditViewController, List<Telephone> telephones)
        {
            _contactEditViewController = contactEditViewController;
            Telephones = telephones.Select(item => item.Clone()).ToList();
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return Telephones.Count;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            ContactEditViewCell cell = tableView.DequeueReusableCell("ContactEditViewCell") as ContactEditViewCell;
            Telephone telephone = Telephones[indexPath.Row];

            cell.SetName(telephone.Name);
            cell.SetEditNameAction((number) => ChooseTelephoneName(telephone));
            cell.SetNumber(telephone.Number);
            cell.SetEditNumberAction((number) => {
                telephone.Number = number;
            });
            cell.SetRemoveIcon(Util.GetSystemImage("trash"));
            cell.SetRemoveNumberAction(() => RemoveTelephone(telephone));
            cell.SetPreferredNumber(telephone.Preferred);
            cell.SetPreferredNumberAction(() => SetPreferedTelephone(telephone));

            return cell;
        }

        public void AddNewTelephone()
        {
            Telephone telephone = new Telephone("Mobile", "", Telephones.Count==0);
            Telephones.Add(telephone);
            _contactEditViewController.RefreshController();
        }

        private void RemoveTelephone(Telephone telephone)
        {
            Telephones.Remove(telephone);
            _contactEditViewController.RefreshController();
        }

        private void SetPreferedTelephone(Telephone telephone)
        {

            for (int i = 0; i < Telephones.Count; ++i)
            {
                if (Telephones[i] == telephone)
                    Telephones[i].Preferred = true;
                else
                    Telephones[i].Preferred = false;
            }
            _contactEditViewController.RefreshController();
        }

        private void ChooseTelephoneName(Telephone telephone)
        {
            string[] telephoneNames = new string[] { "Mobile", "Home", "Work", "Other"};
            UIAlertController alertController = UIAlertController.Create("Contacts", null, UIAlertControllerStyle.ActionSheet);
            foreach (string telephoneName in telephoneNames)
                alertController.AddAction(UIAlertAction.Create(telephoneName, UIAlertActionStyle.Default, (_) => {
                    telephone.Name = telephoneName;
                    _contactEditViewController.RefreshController();
                }));
            alertController.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel, null));
            _contactEditViewController.PresentViewController(alertController, true, null);
        }
    }
}