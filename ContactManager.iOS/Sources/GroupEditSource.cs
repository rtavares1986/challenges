﻿using System;
using System.Collections.Generic;
using System.Linq;
using ContactManager.Core.Models;
using ContactManager.iOS.Services;
using ContactManager.iOS.ViewControllers;
using ContactManager.iOS.Views;
using Foundation;
using UIKit;

namespace ContactManager.iOS.Sources
{
    public class GroupEditSource : UITableViewSource
    {
        private GroupEditViewController _groupEditViewController;
        public List<Contact> Contacts { get; }

        public GroupEditSource(GroupEditViewController groupEditViewController, List<Contact> contacts)
        {
            this._groupEditViewController = groupEditViewController;
            Contacts = contacts.Select(item => item).ToList();
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return Contacts.Count;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            GroupEditViewCell cell = tableView.DequeueReusableCell("GroupEditViewCell") as GroupEditViewCell;
            Contact contact = Contacts[indexPath.Row];

            cell.SetName(contact.Name);
            cell.SetNumber(contact.GetPreferredNumber());
            cell.SetRemoveIcon(Util.GetSystemImage("trash"));
            cell.setRemoveAction(() => RemoveContact(contact));

            return cell;
        }

        public void AddContact(Contact contact)
        {
            if (!Contacts.Contains(contact))
                Contacts.Add(contact);
            Contacts.Sort();
        }

        private void RemoveContact(Contact contact)
        {
            Contacts.Remove(contact);
            Contacts.Sort();
            _groupEditViewController.RefreshController();
        }
    }
}