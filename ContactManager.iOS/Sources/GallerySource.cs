﻿using System;
using System.Collections.Generic;
using ContactManager.iOS.Models;
using ContactManager.iOS.ViewControllers;
using ContactManager.iOS.Views;
using Foundation;
using UIKit;

namespace ContactManager.iOS.Sources
{
    public class GallerySource : UICollectionViewSource
    {
        private GalleryViewController _galleryViewController;
        private List<GalleryItem> _galleryItems;

        public GallerySource(GalleryViewController galleryViewController)
        {
            _galleryViewController = galleryViewController;
            _galleryItems = new List<GalleryItem>();
        }

        public void AddGalleryItem(string title, UIImage image, Action<object> action, object parameter)
        {
            GalleryItem galleryItem = new GalleryItem(title, image, action, parameter);
            _galleryItems.Add(galleryItem);
        }

        public override nint NumberOfSections(UICollectionView collectionView)
        {
            return 1;
        }

        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {
            return _galleryItems.Count;
        }

        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            GalleryViewCell cell = collectionView.DequeueReusableCell("GalleryViewCell", indexPath) as GalleryViewCell;
            GalleryItem galleryItem = _galleryItems[indexPath.Row];

            cell.SetTitle(galleryItem.Title);
            cell.SetImage(galleryItem.Image);

            var tapGestureRecognizer = new UITapGestureRecognizer(() => {
                galleryItem.ImageAction(galleryItem.ImageActionParameter);
                _galleryViewController.NavigationController?.PopViewController(true);
            });
            cell.AddGestureRecognizer(tapGestureRecognizer);

            return cell;
        }
    }
}