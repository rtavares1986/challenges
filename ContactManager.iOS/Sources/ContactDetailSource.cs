﻿using System;
using System.Collections.Generic;
using ContactManager.Core.Models;
using ContactManager.Core.ViewModels;
using ContactManager.iOS.Views;
using Foundation;
using UIKit;

namespace ContactManager.iOS.Sources
{
    public class ContactDetailSource : UITableViewSource
    {
        private ContactDetailViewModel _contactDetailViewModel;
        private List<Telephone> _telephones;

        public ContactDetailSource(ContactDetailViewModel contactDetailViewModel, List<Telephone> telephones)
        {
            _contactDetailViewModel = contactDetailViewModel;
            _telephones = telephones;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _telephones.Count;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            ContactDetailViewCell cell = tableView.DequeueReusableCell("ContactDetailViewCell") as ContactDetailViewCell;
            Telephone telephone = _telephones[indexPath.Row];

            cell.SetName(telephone.Name);
            cell.SetNumber(telephone.Number);
            cell.SetNumberAction(() => _contactDetailViewModel.CallNumber(telephone.Number));
            cell.SetPreferredNumber(telephone.Preferred);

            return cell;
        }
    }
}