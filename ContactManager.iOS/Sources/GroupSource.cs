﻿using System;
using System.Collections.Generic;
using ContactManager.Core.Models;
using ContactManager.Core.ViewModels;
using ContactManager.iOS.Views;
using Foundation;
using UIKit;

namespace ContactManager.iOS.Sources
{
    public class GroupSource : UITableViewSource
    {
        private GroupViewModel _groupViewModel;
        private List<Group> _groups;

        public GroupSource(GroupViewModel groupViewModel)
        {
            _groupViewModel = groupViewModel;
            _groups = _groupViewModel.GetGroups();
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _groups.Count;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            GroupViewCell cell = tableView.DequeueReusableCell("GroupViewCell") as GroupViewCell;
            Group group = _groups[indexPath.Row];

            cell.SetName(group.Name);
            cell.SetMessageAction(() => _groupViewModel.SendGroupSMS(group));
            cell.SetEmailAction(() => _groupViewModel.SendGroupEmail(group));
            cell.SetOnCellTapAction(() => _groupViewModel.OnGroupDetailButtonClick(group));
            cell.SetOnCellLongClickAction(() => _groupViewModel.OnSubMenuClick(group));

            return cell;
        }
    }
}