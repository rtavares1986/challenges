﻿using System;
using ContactManager.iOS.Views;
using Foundation;
using UIKit;

namespace ContactManager.iOS.Sources
{
    public class SubMenuSource : UITableViewSource
    {
        private string[] _options;
        private Action[] _actions;
        private Action _onSelect;

        public SubMenuSource(string[] options, Action[] actions, Action onSelect)
        {
            _options = options;
            _actions = actions;
            _onSelect = onSelect;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _options.Length;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            MenuViewCell cell = tableView.DequeueReusableCell("MenuViewCell") as MenuViewCell;
            String option = _options[indexPath.Row];
            cell.Option = option;
            return cell;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            _actions[indexPath.Row]();
            tableView.DeselectRow(indexPath, true);
            _onSelect();
        }
    }
}