﻿using System;
using ContactManager.Core.Models;
using ContactManager.Core.Services;
using ContactManager.Core.ViewModels;
using ContactManager.iOS.Services;
using ContactManager.iOS.Views;
using Foundation;
using UIKit;

namespace ContactManager.iOS.Sources
{
    public class ContactSource : UITableViewSource
    {
        private readonly ContactViewModel _contactViewModel;
        private ContactFilter _contactFilter;

        public ContactSource(ContactViewModel contactViewModel)
        {
            _contactViewModel = contactViewModel;
            _contactFilter = _contactViewModel.ContactFilter;
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return _contactFilter.Keys.Length;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return _contactFilter.IndexedTableItems[_contactFilter.Keys[section]].Count;
        }

        public override string[] SectionIndexTitles(UITableView tableView)
        {
            return _contactFilter.Keys;
        }

        public override string TitleForHeader(UITableView tableView, nint section)
        {
            return _contactFilter.Keys[section];
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            ContactViewCell cell = tableView.DequeueReusableCell("ContactViewCell") as ContactViewCell;
            Contact contact = _contactFilter.IndexedTableItems[_contactFilter.Keys[indexPath.Section]][indexPath.Row];

            cell.SetPhotoImage(Util.GetPhoto(contact));
            cell.SetName(contact.Name);
            cell.SetNumber(contact.GetPreferredNumber());
            cell.SetCallButtonAction(() => _contactViewModel.CallContact(contact));
            cell.SetOnCellTapAction(() => _contactViewModel.OnContactDetailButtonClick(contact));
            cell.SetOnCellLongClickAction(() => _contactViewModel.OnSubMenuClick(contact));

            return cell;
        }
    }
}