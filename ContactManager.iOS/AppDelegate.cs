﻿using Foundation;
using SimpleInjector;
using UIKit;
using ContactManager.Core.Services;
using ContactManager.Core.Services.Interfaces;
using ContactManager.Core.ViewModels;
using ContactManager.iOS.Services;
using ContactManager.iOS.ViewControllers;

namespace ContactManager.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : UIResponder, IUIApplicationDelegate
    {
        [Export("window")]
        public UIWindow Window { get; set; }

        [Export("application:didFinishLaunchingWithOptions:")]
        public bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            // Override point for customization after application launch.
            // If not required for your application you can safely delete this method

            /// 2. Create a new UIWindow using the windowScene constructor which takes in a window scene.
            var window = new UIWindow();

            var container = Program.Container;
            container.Register<IDataBase, ContactManagerDB>(Lifestyle.Singleton);
            container.Register<IAlertDialog, AlertDialog>(Lifestyle.Singleton);
            container.Register<ISubMenu, SubMenu>(Lifestyle.Singleton);
            container.Register<ContactFilter>(Lifestyle.Singleton);
            container.Register<MainViewModel>(Lifestyle.Singleton);
            container.Register<ContactViewModel>();
            container.Register<ContactDetailViewModel>();
            container.Register<ContactEditViewModel>();
            container.Register<GroupViewModel>();
            container.Register<GroupDetailViewModel>();
            container.Register<GroupEditViewModel>();
            //container.Verify();

            /// 3. Create a view hierarchy programmatically
            //var viewController = new ContactViewController();
            //var navigation = new UINavigationController(rootViewController: viewController);

            /// 4. Set the root view controller of the window with your view controller
            window.RootViewController = new MainViewController(); //navigation;

            /// 5. Set the window and call makeKeyAndVisible()
            Window = window;

            Window.MakeKeyAndVisible();
            return true;
        }
    }
}