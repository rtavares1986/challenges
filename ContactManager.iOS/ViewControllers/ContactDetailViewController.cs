﻿using System.ComponentModel;
using ContactManager.Core.Models;
using ContactManager.Core.ViewModels;
using ContactManager.iOS.Services;
using ContactManager.iOS.Sources;
using CoreLocation;
using Foundation;
using MapKit;
using UIKit;

namespace ContactManager.iOS.ViewControllers
{
    public partial class ContactDetailViewController : UIViewController
    {
        private ContactDetailViewModel _contactDetailViewModel;
        private Contact _contact;

        public ContactDetailViewController(Contact contact) : base("ContactDetailViewController", null)
        {
            _contactDetailViewModel = Program.Container.GetInstance<ContactDetailViewModel>(); ;
            _contact = contact;

            _contactDetailViewModel.PropertyChanged += PropertyChanged;
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            RefreshController();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            EdgesForExtendedLayout = UIRectEdge.None;

            cd_phone_list.RegisterNibForCellReuse(UINib.FromName("ContactDetailViewCell", NSBundle.MainBundle), "ContactDetailViewCell");
            cd_phone_list.AllowsSelection = false;
            cd_phone_list.ReloadData();

            NavigationItem.RightBarButtonItems = new UIBarButtonItem[] {
                    Util.CreateBarItem(systemIcon: "trash", action: () =>
                    {
                        _contactDetailViewModel.OnRemoveContactButtonClick(_contact);
                    }),
                    Util.CreateBarItem(systemIcon: "square.and.arrow.up", action: () => {
                        _contactDetailViewModel.ShareContact(_contact);
                    }),
                    Util.CreateBarItem(systemIcon: "pencil", action: () =>
                    {
                        _contactDetailViewModel.OnEditContactButtonClick(_contact);
                    })
                };
        }

        private void AddMapAnnotation(double latitude, double longitude, string title, bool focus)
        {
            CLLocationCoordinate2D center = new CLLocationCoordinate2D(latitude: latitude, longitude: longitude);
            MKPointAnnotation annotation = new MKPointAnnotation();
            annotation.Coordinate = center;
            annotation.Title = title;
            cd_map.AddAnnotation(annotation);
            if (focus && UIDevice.CurrentDevice.CheckSystemVersion(13, 0))
            {
                MKCoordinateRegion region = new MKCoordinateRegion(center: annotation.Coordinate, span: cd_map.Region.Span);
                cd_map.SetRegion(region, animated: true);
                cd_map.SetCameraZoomRange(new MKMapCameraZoomRange(minDistance: 10, maxDistance: 500), true);
            }
        }

        private void RefreshController()
        {
            cd_image.Image = Util.GetPhoto(_contact);
            cd_phone_list.Source = new ContactDetailSource(_contactDetailViewModel, _contact.Telephones);
            cd_name.Text = _contact.Name;
            cd_email.SetTitle(_contact.Email, UIControlState.Normal);
            cd_email.TouchUpInside += (sender, e) => _contactDetailViewModel.SendEmail(_contact);
            cd_address.Text = _contact.Address;

            cd_map.RemoveAnnotations();
            AddMapAnnotation(_contact.Location.Item1, _contact.Location.Item2, _contact.Name, true);
        }

        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "PopViewController":
                    NavigationController?.PopViewController(true);
                    break;
            }
        }
    }
}