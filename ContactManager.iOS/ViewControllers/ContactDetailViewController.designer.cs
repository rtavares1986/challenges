// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ContactManager.iOS.ViewControllers
{
	[Register ("ContactDetailViewController")]
	partial class ContactDetailViewController
	{
		[Outlet]
		UIKit.UILabel cd_address { get; set; }

		[Outlet]
		UIKit.UIButton cd_email { get; set; }

		[Outlet]
		UIKit.UIImageView cd_image { get; set; }

		[Outlet]
		MapKit.MKMapView cd_map { get; set; }

		[Outlet]
		UIKit.UILabel cd_name { get; set; }

		[Outlet]
		UIKit.UITableView cd_phone_list { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (cd_address != null) {
				cd_address.Dispose ();
				cd_address = null;
			}

			if (cd_image != null) {
				cd_image.Dispose ();
				cd_image = null;
			}

			if (cd_map != null) {
				cd_map.Dispose ();
				cd_map = null;
			}

			if (cd_name != null) {
				cd_name.Dispose ();
				cd_name = null;
			}

			if (cd_email != null) {
				cd_email.Dispose ();
				cd_email = null;
			}

			if (cd_phone_list != null) {
				cd_phone_list.Dispose ();
				cd_phone_list = null;
			}
		}
	}
}
