﻿using System.Collections.Generic;
using System.ComponentModel;
using ContactManager.Core.Models;
using ContactManager.Core.ViewModels;
using ContactManager.iOS.Services;
using ContactManager.iOS.Sources;
using Foundation;
using UIKit;

namespace ContactManager.iOS.ViewControllers
{
    public partial class ContactEditViewController : UIViewController
    {
        private ContactEditViewModel _contactEditViewModel;
        private Contact _contact;
        private ContactEditSource _contactEditSource;
        private (double, double) _currentLocation;
        private string photo;

        public ContactEditViewController(Contact contact) : base("ContactEditViewController", null)
        {
            _contactEditViewModel = Program.Container.GetInstance<ContactEditViewModel>();
            _contact = contact;
            _currentLocation = (0, 0);

            _contactEditViewModel.PropertyChanged += PropertyChanged;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            EdgesForExtendedLayout = UIRectEdge.None;

            _contactEditSource = new ContactEditSource(this, _contact.Telephones);
            ce_phone_list.RegisterNibForCellReuse(UINib.FromName("ContactEditViewCell", NSBundle.MainBundle), "ContactEditViewCell");
            ce_phone_list.Source = _contactEditSource;
            ce_phone_list.AllowsSelection = false;
            ce_phone_list.ReloadData();

            LoadPhoto();
            ce_photo.UserInteractionEnabled = true;
            ce_photo.AddGestureRecognizer(new UITapGestureRecognizer(() => GetPhotoFrom()));
            ce_name.Text = _contact.Name;
            ce_email.Text = _contact.Email;
            ce_location.TouchUpInside += (sender, e) => AssignAddress();
            ce_address.Text = _contact.Address;
            ce_add_phone.TouchUpInside += (sender, e) => _contactEditSource.AddNewTelephone();
            ce_location.SetImage(Util.GetSystemImage("location"), UIControlState.Normal);

            NavigationItem.RightBarButtonItem = Util.CreateBarItem(title: "Save", action: () => SaveContact());

            if (ce_address.Text.Length == 0)
                AssignAddress();
        }

        private void LoadPhoto() {
            if (_contact.Photo.Length != 0)
                ce_photo.Image = UIImage.FromFile(_contact.Photo);
            else {
                if (photo != null)
                    ce_photo.Image = UIImage.FromFile(photo);
                else
                    ce_photo.Image = UIImage.FromBundle("contact_icon");
            }
        }

        async private void GetPhotoFrom()
        {
            photo = await CMImage.GetPhotoFrom();
            if (photo != null)
                ce_photo.Image = UIImage.FromFile(photo);
        }

        private async void AssignAddress() {
            _currentLocation = await CMLocation.GetCurrentLocation();
            if (_currentLocation == (0, 0))
                return;
            string address = await CMLocation.GetAddress(_currentLocation.Item1, _currentLocation.Item2);
            ce_address.Text = address;
        }

        private void SaveContact()
        {
            List<Telephone> telephoneList = _contactEditSource.Telephones;

            if (ce_name.Text.Length > 0 && ce_email.Text.Length > 0 && ce_address.Text.Length > 0 && telephoneList.Count > 0)
            {
                _contact.Photo = photo ?? "";
                _contact.Name = ce_name.Text;
                _contact.Email = ce_email.Text;
                _contact.Address = ce_address.Text;
                _contact.Telephones = telephoneList;
                if (_currentLocation != (0, 0))
                    _contact.Location = _currentLocation;
                _contactEditViewModel.SaveContact(_contact);
            }
            else
                Util.MessageDialog("Incomplete Information", "Check if it's missing any information on the form!");
        }

        public void RefreshController()
        {
            ce_phone_list.ReloadData();
        }

        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "PopViewController":
                    NavigationController?.PopViewController(true);
                    break;
            }
        }
    }
}