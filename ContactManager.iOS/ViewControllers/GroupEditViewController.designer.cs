// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ContactManager.iOS.ViewControllers
{
	[Register ("GroupEditViewController")]
	partial class GroupEditViewController
	{
		[Outlet]
		UIKit.UIButton addContact { get; set; }

		[Outlet]
		UIKit.UITableView contactList { get; set; }

		[Outlet]
		UIKit.UITextField contactName { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (contactList != null) {
				contactList.Dispose ();
				contactList = null;
			}

			if (contactName != null) {
				contactName.Dispose ();
				contactName = null;
			}

			if (addContact != null) {
				addContact.Dispose ();
				addContact = null;
			}
		}
	}
}
