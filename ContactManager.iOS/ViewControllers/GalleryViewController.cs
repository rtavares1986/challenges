﻿using System;
using Foundation;
using UIKit;
using ContactManager.Core.Models;
using ContactManager.iOS.Services;
using ContactManager.iOS.Sources;

namespace ContactManager.iOS.ViewControllers
{
    public partial class GalleryViewController : UIViewController
    {
        private Contact[] _contacts;
        private Action<object> _action;

        public GalleryViewController(Contact[] contacts, Action<object> action) : base("GalleryViewController", null)
        {
            _contacts = contacts;
            _action = action;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            EdgesForExtendedLayout = UIRectEdge.None;

            GallerySource gallerySource = new GallerySource(this);
            foreach(Contact contact in _contacts)
                gallerySource.AddGalleryItem(contact.Name, Util.GetPhoto(contact), _action, (object) contact);

            GalleryCollection.RegisterNibForCell(UINib.FromName("GalleryViewCell", NSBundle.MainBundle), "GalleryViewCell");
            GalleryCollection.Source = gallerySource;
        }
    }
}