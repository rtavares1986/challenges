﻿using System.ComponentModel;
using Foundation;
using UIKit;
using ContactManager.Core.Models;
using ContactManager.Core.ViewModels;
using ContactManager.iOS.Services;
using ContactManager.iOS.Sources;

namespace ContactManager.iOS.ViewControllers
{
    public partial class GroupEditViewController : UIViewController
    {
        private GroupEditViewModel _groupEditViewModel;
        private Group _group;
        private GroupEditSource _groupEditSource;

        public GroupEditViewController(Group group) : base("GroupEditViewController", null)
        {
            _groupEditViewModel = Program.Container.GetInstance<GroupEditViewModel>();
            _group = group;

            _groupEditViewModel.PropertyChanged += PropertyChanged;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            EdgesForExtendedLayout = UIRectEdge.None;

            contactName.Text = _group.Name;
            addContact.TouchUpInside += (sender, e) => AddContact();

            _groupEditSource = new GroupEditSource(this, _group.Contacts);
            contactList.RegisterNibForCellReuse(UINib.FromName("GroupEditViewCell", NSBundle.MainBundle), "GroupEditViewCell");
            contactList.Source = _groupEditSource;
            contactList.AllowsSelection = false;
            RefreshController();

            NavigationItem.RightBarButtonItem = Util.CreateBarItem(title: "Save", action: () => SaveGroup());
        }

        private void AddContact()
        {
            var galleryViewController = new GalleryViewController(_groupEditViewModel.GetAllContacts(), (contact) => {
                _groupEditSource.AddContact((Contact) contact);
                RefreshController();
            });
            NavigationController?.PushViewController(galleryViewController, true);
        }

        private void SaveGroup()
        {
            if (contactName.Text.Length > 0)
            {
                _group.Name = contactName.Text;
                _group.Contacts = _groupEditSource.Contacts;
                _groupEditViewModel.SaveGroup(_group);
                NavigationController?.PopViewController(true);
            }
            else
                Util.MessageDialog("Incomplete Information", "Check if it's missing any information on the form!");
        }

        public void RefreshController()
        {
            contactList.ReloadData();
        }

        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "PopViewController":
                    NavigationController?.PopViewController(true);
                    break;
            }
        }
    }
}