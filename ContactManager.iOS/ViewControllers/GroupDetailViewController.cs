﻿using System.ComponentModel;
using Foundation;
using UIKit;
using ContactManager.Core.Models;
using ContactManager.Core.ViewModels;
using ContactManager.iOS.Services;
using ContactManager.iOS.Sources;

namespace ContactManager.iOS.ViewControllers
{
    public partial class GroupDetailViewController : UIViewController
    {
        private GroupDetailViewModel _groupDetailViewModel;
        private Group _group;

        public GroupDetailViewController(Group group) : base("GroupDetailViewController", null)
        {
            _groupDetailViewModel = Program.Container.GetInstance<GroupDetailViewModel>();
            _group = group;

            _groupDetailViewModel.PropertyChanged += PropertyChanged;
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            RefreshViewController();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            EdgesForExtendedLayout = UIRectEdge.None;

            contactList.RegisterNibForCellReuse(UINib.FromName("GroupDetailViewCell", NSBundle.MainBundle), "GroupDetailViewCell");
            contactList.AllowsSelection = false;

            NavigationItem.RightBarButtonItems = new UIBarButtonItem[] {
                Util.CreateBarItem(systemIcon: "trash", action: () => {
                    _groupDetailViewModel.OnRemoveGroupButtonClick(_group);
                }),
                Util.CreateBarItem(systemIcon: "pencil", action: () => {
                    _groupDetailViewModel.OnEditGroupButtonClick(_group);
                })
            };

        }

        private void RefreshViewController()
        {
            groupName.Text = _group.Name;
            contactList.Source = new GroupDetailSource(_groupDetailViewModel, _group.Contacts);
        }

        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "PopViewController":
                    NavigationController?.PopViewController(true);
                    break;
            }
        }
    }
}