// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ContactManager.iOS.ViewControllers
{
	[Register ("GalleryViewController")]
	partial class GalleryViewController
	{
		[Outlet]
		UIKit.UICollectionView GalleryCollection { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (GalleryCollection != null) {
				GalleryCollection.Dispose ();
				GalleryCollection = null;
			}
		}
	}
}
