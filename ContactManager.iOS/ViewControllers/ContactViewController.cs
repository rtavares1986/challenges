﻿using System.ComponentModel;
using Foundation;
using UIKit;
using ContactManager.Core.ViewModels;
using ContactManager.iOS.Services;
using ContactManager.iOS.Sources;

namespace ContactManager.iOS.ViewControllers
{
    public partial class ContactViewController : UIViewController
    {
        private ContactViewModel _contactViewModel;

        public ContactViewController() : base("ContactViewController", null)
        {
            _contactViewModel = Program.Container.GetInstance<ContactViewModel>();

            _contactViewModel.PropertyChanged += PropertyChanged;
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            _contactViewModel.RefreshController();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            EdgesForExtendedLayout = UIRectEdge.None;

            UIBarButtonItem groupIcon = Util.CreateBarItem(
                customIcon: "group_icon",
                action: () => _contactViewModel.OnGroupButtonClick()
            );
            NavigationItem.LeftBarButtonItem = groupIcon;

            UIBarButtonItem addContact = Util.CreateBarItem(
                title: "New",
                action: () => _contactViewModel.OnAddContactButtonClick()
            );
            NavigationItem.RightBarButtonItem = addContact;

            searchBar.LeftView = new UIImageView(Util.GetSystemImage("magnifyingglass"));
            searchBar.LeftViewMode = UITextFieldViewMode.Always;
            searchBar.AllEditingEvents += (sender, e) => _contactViewModel.OnSearchBarTextChange(searchBar.Text);

            list.RegisterNibForCellReuse(UINib.FromName("ContactViewCell", NSBundle.MainBundle), "ContactViewCell");
            list.Source = new ContactSource(_contactViewModel);

            NSIndexSet section = new NSIndexSet();
            list.InsertSections(section, UITableViewRowAnimation.None);
        }

        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "RefreshController":
                    list.ReloadData();
                    break;
            }
        }
    }
}