// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ContactManager.iOS.ViewControllers
{
	[Register ("GroupViewController")]
	partial class GroupViewController
	{
		[Outlet]
		UIKit.UITableView list { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (list != null) {
				list.Dispose ();
				list = null;
			}
		}
	}
}
