﻿using UIKit;
using ContactManager.Core.Models;
using ContactManager.Core.ViewModels;

namespace ContactManager.iOS.ViewControllers
{
    public class MainViewController : UINavigationController
    {
        private MainViewModel _mainViewModel;

        public MainViewController() : base(rootViewController: new ContactViewController())
        {
            _mainViewModel = Program.Container.GetInstance<MainViewModel>();

            _mainViewModel.NavigateTo = NavigateTo;
        }

        private void NavigateTo(object sender, Navigator e)
        {
            switch (e.Key)
            {
                case NavigatorDestiny.GroupViewController:
                    NavigateToGroups();
                    break;
                case NavigatorDestiny.ContactDetailViewController:
                    NavigateToContactDetail((Contact)e.Parameter);
                    break;
                case NavigatorDestiny.ContactEditViewController:
                    NavigateToContactEdit((Contact)e.Parameter);
                    break;
                case NavigatorDestiny.NewContactViewController:
                    NavigateToNewContact();
                    break;
                case NavigatorDestiny.GroupDetailViewController:
                    NavigateToGroupDetail((Group)e.Parameter);
                    break;
                case NavigatorDestiny.GroupEditViewController:
                    NavigateToGroupEdit((Group)e.Parameter);
                    break;
                case NavigatorDestiny.NewGroupViewController:
                    NavigateToNewGroup();
                    break;
            }
        }

        private void NavigateToGroups()
        {
            var groupViewController = new GroupViewController();
            PushViewController(groupViewController, animated: true);
        }

        private void NavigateToContactDetail(Contact contact)
        {
            var contactDetailViewController = new ContactDetailViewController(contact);
            PushViewController(contactDetailViewController, animated: true);
        }

        private void NavigateToContactEdit(Contact contact)
        {
            var contactEditViewController = new ContactEditViewController(contact);
            PushViewController(contactEditViewController, animated: true);
        }

        private void NavigateToNewContact()
        {
            var contactEditViewController = new ContactEditViewController(new Contact());
            PushViewController(contactEditViewController, animated: true);
        }

        private void NavigateToGroupDetail(Group group)
        {
            var contactDetailViewController = new GroupDetailViewController(group);
            PushViewController(contactDetailViewController, animated: true);
        }

        private void NavigateToGroupEdit(Group group)
        {
            var contactEditViewController = new GroupEditViewController(group);
            PushViewController(contactEditViewController, animated: true);
        }

        private void NavigateToNewGroup()
        {
            var contactEditViewController = new GroupEditViewController(new Group(""));
            PushViewController(contactEditViewController, animated: true);
        }
    }
}