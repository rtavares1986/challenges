// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace ContactManager.iOS.ViewControllers
{
	[Register ("ContactEditViewController")]
	partial class ContactEditViewController
	{
		[Outlet]
		UIKit.UIButton ce_add_phone { get; set; }

		[Outlet]
		UIKit.UITextField ce_address { get; set; }

		[Outlet]
		UIKit.UITextField ce_email { get; set; }

		[Outlet]
		UIKit.UIButton ce_location { get; set; }

		[Outlet]
		UIKit.UITextField ce_name { get; set; }

		[Outlet]
		UIKit.UITableView ce_phone_list { get; set; }

		[Outlet]
		UIKit.UIImageView ce_photo { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ce_add_phone != null) {
				ce_add_phone.Dispose ();
				ce_add_phone = null;
			}

			if (ce_location != null) {
				ce_location.Dispose ();
				ce_location = null;
			}

			if (ce_address != null) {
				ce_address.Dispose ();
				ce_address = null;
			}

			if (ce_email != null) {
				ce_email.Dispose ();
				ce_email = null;
			}

			if (ce_name != null) {
				ce_name.Dispose ();
				ce_name = null;
			}

			if (ce_phone_list != null) {
				ce_phone_list.Dispose ();
				ce_phone_list = null;
			}

			if (ce_photo != null) {
				ce_photo.Dispose ();
				ce_photo = null;
			}
		}
	}
}
