﻿using System.ComponentModel;
using Foundation;
using UIKit;
using ContactManager.Core.ViewModels;
using ContactManager.iOS.Services;
using ContactManager.iOS.Sources;

namespace ContactManager.iOS.ViewControllers
{
    public partial class GroupViewController : UIViewController
    {
        private GroupViewModel _groupViewModel;

        public GroupViewController() : base("GroupViewController", null)
        {
            _groupViewModel = Program.Container.GetInstance<GroupViewModel>();

            _groupViewModel.PropertyChanged += PropertyChanged;
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            RefreshController();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            EdgesForExtendedLayout = UIRectEdge.None;

            list.RegisterNibForCellReuse(UINib.FromName("GroupViewCell", NSBundle.MainBundle), "GroupViewCell");
            list.Source = new GroupSource(_groupViewModel);

            NavigationItem.RightBarButtonItem = Util.CreateBarItem(title: "New", action: () =>
            {
                _groupViewModel.OnAddGroupButtonClick();
            });
        }

        public void RefreshController()
        {
            list.ReloadData();
        }

        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "RefreshController":
                    list.ReloadData();
                    break;
            }
        }
    }
}