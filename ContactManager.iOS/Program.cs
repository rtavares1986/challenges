﻿using SimpleInjector;

namespace ContactManager.iOS
{
    public static class Program
    {
        public static Container Container { get; } = CreateContainer();
        private static Container CreateContainer()
        {
            var container = new Container();
            container.Options.AllowOverridingRegistrations = true;
            return container;
        }
    }
}
